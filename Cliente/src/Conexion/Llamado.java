/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import Objeto.ObjetoCorreo;
import com.sun.istack.internal.logging.Logger;
import com.sun.security.ntlm.Client;
import java.net.*;

import java.io.*;
import java.util.logging.Level;

/**
 *
 *
 *
 * @author Jorge V
 *
 */
public class Llamado {

    final String HOST = "localhost";
    final int PUERTO = 1234;
    protected Socket socket;
    protected ObjectOutputStream dataObject;
    
//Cliente
    public void initClient() /*ejecuta este metodo para correr el cliente */ {

        try {
            this.socket = new Socket(HOST, PUERTO);
            this.dataObject = new ObjectOutputStream(this.socket.getOutputStream());
            this.dataObject.writeObject(new ObjetoCorreo("Este es mi mensaje","jrlinares@funsanmateo.edu.co"));
            System.out.println("Objeto enviado");
            this.socket.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }

}
